import "./style.scss";

import CaptureScreen from "../CaptureScreen/index";
import WelcomeScreen from "../WelcomeScreen/index";
import useCaptureStream from "./useCaptureStream";
import { useCallback, useState } from "react";
import DownloadScreen from "../DownloadScreen/index";

const App = () => {
    const { captureStream, request, stop } = useCaptureStream();
    const [recordedVideo, setRecordedVideo] = useState<string | null>(null);

    const handleFinish = useCallback((chunks: Blob[]) => {
        stop();
        const file = new File(chunks, `recording.webm`);
        setRecordedVideo(URL.createObjectURL(file));
    }, [stop]);

    const handleRestart = useCallback(() => {
        if (recordedVideo) {
            URL.revokeObjectURL(recordedVideo);
        }
        setRecordedVideo(null);
    }, [recordedVideo]);

    if (recordedVideo) {
        return <DownloadScreen recordedVideo={recordedVideo} onRestart={handleRestart} />;
    }
    
    if (captureStream) {
        return (
            <CaptureScreen stream={captureStream} onFinish={handleFinish} />
        );
    }
    
    return <WelcomeScreen onRequest={request} />;
};

export default App;
