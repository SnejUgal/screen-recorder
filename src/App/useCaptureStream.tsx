import { useCallback, useState } from "react";

const useCaptureStream = () => {
    const [captureStream, setCaptureStream] = useState<MediaStream | null>(
        null
    );

    const request = useCallback(async () => {
        setCaptureStream(
            await navigator.mediaDevices.getDisplayMedia({
                video: true,
                audio: false,
            })
        );
    }, []);

    const stop = useCallback(() => {
        if (!captureStream) {
            return;
        }

        for (const track of captureStream.getTracks()) {
            track.stop();
            captureStream.removeTrack(track);
        }
        setCaptureStream(null);
    }, [captureStream]);

    return { captureStream, request, stop };
};

export default useCaptureStream;
