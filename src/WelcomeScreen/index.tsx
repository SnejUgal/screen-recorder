import "./style.scss";

interface Props {
    onRequest(): Promise<void>;
}

const WelcomeScreen = ({ onRequest }: Props) => {
    return (
        <main className="welcome screen">
            <button className="welcome_startRecording" onClick={onRequest}>
                Start recording
            </button>
        </main>
    );
};

export default WelcomeScreen;
