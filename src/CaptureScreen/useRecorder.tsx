import { useEffect, useRef, useState } from "react";

interface Props {
    stream: MediaStream;
    onFinish(chunks: Blob[]): void;
}

const useRecorder = ({ stream, onFinish }: Props) => {
    const chunks = useRef<Blob[]>([]);

    const [recorder] = useState(() => {
        const recorder = new MediaRecorder(stream);

        recorder.addEventListener(`dataavailable`, (event) => {
            chunks.current.push(event.data);
        });

        recorder.start();
        return recorder;
    });

    useEffect(() => {
        const handler = () => onFinish(chunks.current);

        recorder.addEventListener(`stop`, handler);
        return () => recorder.removeEventListener(`stop`, handler);
    }, [recorder, onFinish]);

    return recorder;
};

export default useRecorder;
