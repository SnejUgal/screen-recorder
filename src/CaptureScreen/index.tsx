import "./style.scss";

import { useCallback } from "react";
import useRecorder from "./useRecorder";

interface Props {
    stream: MediaStream;
    onFinish(chunks: never): void;
}

const CaptureScreen = ({ stream, onFinish }: Props) => {
    const recorder = useRecorder({ stream, onFinish });

    const handleMount = useCallback((video: HTMLVideoElement | null) => {
        if (!video) {
            return;
        }

        video.srcObject = stream;
    }, []);

    const handleFinish = useCallback(() => {
        recorder.stop();
    }, [recorder]);

    return (
        <div className="capture screen">
            <header className="capture_header">
                <h1 className="capture_title">Recording your screen!</h1>
                <button
                    className="button capture_finish"
                    type="button"
                    onClick={handleFinish}
                >
                    Finish
                </button>
            </header>
            <div className="stream">
                <video
                    className="stream_video"
                    ref={handleMount}
                    autoPlay={true}
                />
            </div>
        </div>
    );
};

export default CaptureScreen;
