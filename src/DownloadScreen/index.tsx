import "./style.scss";

interface Props {
    recordedVideo: string;
    onRestart(): void;
}

const DownloadScreen = ({ recordedVideo, onRestart }: Props) => {
    return (
        <main className="download screen">
            <header className="download_header">
                <h1 className="download_title">Recording finished!</h1>
                <button className="download_restart" type="button" onClick={onRestart}>
                    Record another one
                </button>
                <a className="button download_button" href={recordedVideo} download="video.webm">Download the recording</a>
            </header>
            <div className="stream">
                <video className="stream_video" src={recordedVideo} controls={true} />
            </div>
        </main>
    )
};

export default DownloadScreen;
